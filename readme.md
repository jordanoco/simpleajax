# simpleajax
Simpleajax is a single file library that makes using AJAX in a webpage easy. It was created because I have seen many large websites using JQuery just for the ability to use AJAX requests, when this is completely uncessary. It is meant to be easy to read, as to demystify how other libraries may approach this problem. It is also smaller than importing larger libraries, provided you only need to do simple tasks.

## Documentation

#### `AJAX.get(url, parameters, callback)`
Performs a GET request on the given URL
* `url [string]` is the url to perform the request on
* `parameters [object]` is an object of parameters and values to perform the request with
* `callback [function]` is a function called when the requst is done, with the response being the only parameter

``` javascript
/* request an API for user information */
AJAX.get("https://someapi.org/users", {username: "dave101"}, (res) => {
    console.log(res.firstName)
});
```

#### `AJAX.post(url, parameters, callback)`
Performs a POST request on the given URL. The method used is `application/x-www-form-urlencoded`. 
* `url [string]` is the url to perform the request on
* `parameters [object]` is an object of parameters and values to perform the request with
* `callback [function]` is a function called when the requst is done, with the response being the only parameter

``` javascript
/* request an API to create a blog post */
AJAX.get("https://someblogapi.org", {title: "hello!", content: "This is a blog"}, (res) => {
    console.log(res.success);
});
```

## How to Use
Download and place a copy in your working directory and link in your HTML. Use as necessary.