/* keep all the functions out of the global object */
var AJAX = {};
AJAX.helper = {};

/* 
 * If there is no native XMLHttpRequest (ie<6), return the correct equivilent, 
 * else just return an XMLHttpRequest
*/
AJAX.helper.getXMLHttpRequest = function () {
    if (window.XMLHttpRequest) return new XMLHttpRequest();
    else return new ActiveXObject("Microsoft.XMLHTTP");
};

/* 
 * Get the keys in an object in a way that supports older versions of IE
 */
AJAX.helper.objectKeys = function (obj) {
    var keys = [], 
        property = undefined;

    if (object !== Object(object)) return [];
    for (property in object) {
        if (Object.prototype.hasOwnProperty.call(object, property)) {
            keys.push(property);
        }
    }

    return keys;
};

/**
 * Performs a GET request on the given url, with the specifed parameters.
 * @param {String} url the url to perform the request on.
 * @param {Object} parameters an object of params and values to request the
 * server with.
 * @param {Function} callback a function called with one parameter, the 
 * response the server provides.
 */
AJAX.get = function (url, parameters, callback) {
    var keys = AJAX.helper.objectKeys(parameters);

    if (keys.length > 0) url += "?";
    for (var i = 0; i < keys.length; i++) {
        if (i !== 0) url += "&";
        url += keys[i] + "=" + parameters[keys[i]];
    }

    var xhttp = AJAX.helper.getXMLHttpRequest();
    xhttp.open("GET", url, true);
    xhttp.onreadystatechange = function () {
        if (this.readystate === 4 && this.status === 200) {
            callback(this.responseText);
        }
    };
    xhttp.send();
};

/**
 * Performs a POST request on the given url, with the specifed parameters
 * @param {String} url the url to perform the request on.
 * @param {Object} parameters an object of params and values to request the
 * server with.
 * @param {Function} callback a function called with one parameter, the 
 * response the server provides.
 */
AJAX.post = function (url, parameters, callback) {
    var xhttp = AJAX.helper.getXMLHttpRequest(),
        keys = AJAX.helper.objectKeys(parameters),
        paramstring = "";

    for (var i = 0; i < keys.length; i++) {
        if (i !== 0) paramstring += "&";
        paramstring += keys[i] + "=" + parameters[keys[i]];
    }

    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.onreadystatechange = function () {
        if (this.readystate === 4 && this.status === 200) {
            callback(this.responseText);
        }
    };

    xhttp.send(paramstring);
};